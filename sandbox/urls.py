from django.conf import settings
from django.urls import include, re_path
from django.contrib import admin

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from rest_framework.routers import DefaultRouter
from wagtail_spa_integration.views import RedirectViewSet, sitemap
from .test_views import CreateFixturesView
from .api import api_router

router = DefaultRouter()
router.register(r'redirects', RedirectViewSet, basename='redirects')

urlpatterns = [
    re_path(r'^django-admin/', admin.site.urls),

    re_path(r'^sitemap\.xml$', sitemap),
    re_path(r'^admin/', include(wagtailadmin_urls)),
    re_path(r'^documents/', include(wagtaildocs_urls)),
    re_path(r'^test-fixture/', CreateFixturesView.as_view()),
    re_path(r'^api/v2/', api_router.urls),
    re_path(r'^api/', include(router.urls)),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    re_path(r'', include(wagtail_urls)),

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    re_path(r'^pages/', include(wagtail_urls)),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
